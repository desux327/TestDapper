-- Database: testbase

-- DROP DATABASE IF EXISTS testbase;

CREATE DATABASE testbase
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Kazakhstan.1251'
    LC_CTYPE = 'Russian_Kazakhstan.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;
	
CREATE TABLE driver
(id BIGSERIAL NOT NULL PRIMARY KEY,
name VARCHAR(50) NOT NULL);

CREATE TABLE auto
(id BIGSERIAL NOT NULL PRIMARY KEY,
brand VARCHAR(50) NOT NULL,
model VARCHAR(50) NOT NULL
);

ALTER TABLE driver ADD auto_id BIGINT REFERENCES auto (id);

ALTER TABLE driver ADD UNIQUE(auto_id);

INSERT INTO driver (name) VALUES ('Nikolay');
INSERT INTO driver (name) VALUES ('Alexander');
INSERT INTO driver (name) VALUES ('Alexey');
INSERT INTO driver (name) VALUES ('Viktor');
INSERT INTO driver (name) VALUES ('Sergey');
INSERT INTO auto (brand, model) VALUES ('Nissan', 'Pathfinder');
INSERT INTO auto (brand, model) VALUES ('Toyota', 'Corolla');
INSERT INTO auto (brand, model) VALUES ('BMW', 'X5');
INSERT INTO auto (brand, model) VALUES ('Lada', 'Granta');
INSERT INTO auto (brand, model) VALUES ('Kia', 'Rio');

UPDATE driver SET auto_id = 1 WHERE id = 2;
UPDATE driver SET auto_id = 3 WHERE id = 4;
UPDATE driver SET auto_id = 2 WHERE id = 3;
UPDATE driver SET auto_id = 4 WHERE id = 1;
UPDATE driver SET auto_id = 5 WHERE id = 5;

SELECT * FROM driver;